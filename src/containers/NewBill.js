import { ROUTES_PATH } from "../constants/routes.js";
import Logout from "./Logout.js";

export default class NewBill {
	constructor({ document, onNavigate, firestore, localStorage }) {
		console.assert(firestore);
		this.document = document;
		this.onNavigate = onNavigate;
		this.firestore = firestore;
		const formNewBill = document.getElementById('form-new-bill');
		formNewBill.addEventListener("submit", this.handleSubmit);
		this.fileInput = document.getElementById('file');
		this.fileInput.addEventListener("change", this.handleChangeFileEvent);
		this.fileUrl = null;
		new Logout({ document, localStorage, onNavigate });
	}


	handleChangeFileEvent = e =>
	{
		this.handleChangeFile( [...this.fileInput.files] )
	}


	handleChangeFile = files =>
	{
		if( files.length === 0 )
		{
			delete this.file;
			return;
		}

		if( files.length > 1 )
		{
			alert( "Vous pouvez téléverser qu'un seul justificatif." );
			this.fileInput.value = "";
			return;
		}

		this.file = files[0];

		var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;

		if( !allowedExtensions.exec(this.file.name) )
		{
			alert("Types de fichiers acceptés: jpg, jpeg, png.");
			this.fileInput.value = "";
		}
	};



	// is file input required? What if there is none?
	//
	handleSubmit = async e =>
	{
		e.preventDefault();

		if( !this.file )
		{
			alert( "Veuillez téléverser votre justificatif de frais" );
			return;
		}

		const email = JSON.parse(localStorage.getItem("user")).email;

		let snapshot = await this.firestore.storage
			.ref(`justificatifs/${this.file.name}`)
			.put(this.file)
		;


		this.fileUrl = await snapshot.ref.getDownloadURL();

		const bill = {
			email,
			type      : document.getElementById( "expense-type" ).value,
			name      : document.getElementById( "expense-name" ).value,
			amount    : parseInt( document.getElementById( "amount" ).value ),
			date      : document.getElementById( "datepicker" ).value,
			vat       : document.getElementById( "vat" ).value,
			pct       : parseInt( document.getElementById( "pct" ).value ) || 20,
			commentary: document.getElementById( "commentary" ).value,
			fileUrl   : this.fileUrl,
			fileName  : this.file.name,
			status    : "pending",
		};

		await this.firestore.bills().add(bill);
		this.onNavigate(ROUTES_PATH["Bills"]);
	};
}
