import { screen } from "@testing-library/dom";
import NewBillUI from "../views/NewBillUI.js";
import NewBill from "../containers/NewBill.js";
import firestore from "../app/Firestore";
import { localStorageMock } from "../__mocks__/localStorage.js";

jest.mock("../app/Firestore");

const USER = JSON.stringify
({
	type     :"Employee",
	email    :"a@a",
	password :"azerty",
	status   :"connected"
});

describe("Given I am connected as an employee", () => {
	describe("When I am on NewBill Page", () => {
		describe("When I select a file", () => {

			let controller;
			let file;

			beforeEach(() =>
			{
				const html = NewBillUI();
				document.body.innerHTML = html;
				controller = new NewBill({document, onNavigate: null, firestore, localStorage});

				const fileContents = 'file contents';
				file               = new Blob([fileContents], {type : 'image/jpeg'} );
				file.name          = "test_name.jpg";
			});


			// - should unset when file upload is empty
			// - should set name if there is a file
			// - should reject any files that are not jpg/jpeg/png
			// - should reject several files.
			//
			test( "it's name should be stored", () =>
			{
				controller.handleChangeFile( [file] );
				expect( controller.file.name ).toEqual( "test_name.jpg" );
			});



			test( "it's name should be cleared if there is no file", () =>
			{
				controller.handleChangeFile( [file] );
				controller.handleChangeFile( [] );
				expect( controller.file ).toEqual( undefined );
			});



			test( "Then it should reject extensions that arent jpg/png", () =>
			{
				file.name = "bla.pdf";

				let alert = jest.spyOn( window, 'alert' ).mockImplementation( text =>
				{
					expect( text ).toEqual( "Types de fichiers acceptés: jpg, jpeg, png." );
				});

				controller.handleChangeFile( [file] );

				expect( alert ).toHaveBeenCalled();
			});



			test( "Then it should reject multiple files", () =>
			{
				let alert = jest.spyOn( window, 'alert' ).mockImplementation( text =>
				{
					expect( text ).toEqual( "Vous pouvez téléverser qu'un seul justificatif." );
				});

				controller.handleChangeFile( [file, file] );

				expect( alert ).toHaveBeenCalled();
			});
		});



		// - If no file is selected, refuse to submit
		// - upload the file to the backend and store the bill with correct values in the backend.
		// - go back to the bills page.
		//
		describe( "When I submit the NewBill form", () => {

			let controller;

			beforeEach(() =>
			{
				Object.defineProperty(window, "localStorage", { value: localStorageMock });
				window.localStorage.setItem("user", USER);

				const html = NewBillUI();
				document.body.innerHTML = html;

				controller = new NewBill({document, onNavigate: jest.fn(), firestore, localStorage});
			});


			test( "Then it should reject when no file is selected", async () =>
			{
				let alert = jest.spyOn( window, 'alert' ).mockImplementation( text =>
				{
					expect( text ).toEqual( "Veuillez téléverser votre justificatif de frais" );
				});

				await controller.handleSubmit( new Event("Mock submit") );

				expect( alert ).toHaveBeenCalled();
			});


			test( "Then it should correctly send the bill to the backend and move to the bills page", async () =>
			{
				const fileContents = 'file contents';
				const file         = new Blob([fileContents], {type : 'image/jpeg'} );
				file.name          = "test_name.jpg";
				controller.file    = file;

				await controller.handleSubmit( new Event("Mock submit") );

				expect( firestore.path     ).toEqual( "justificatifs/test_name.jpg" );
				expect( firestore.uploaded ).toEqual( file );
				expect( controller.onNavigate ).toHaveBeenCalledWith( "#employee/bills" );
			});

		});

	});
});
