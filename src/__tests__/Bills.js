import { screen } from "@testing-library/dom";
import BillsUI from "../views/BillsUI.js";
import Router from "../app/Router.js";
import { bills } from "../fixtures/bills.js";
import { ROUTES, ROUTES_PATH } from "../constants/routes";
import { localStorageMock } from "../__mocks__/localStorage.js";
import firebase from "../__mocks__/firebase";

jest.mock("../app/Firestore");

const USER = JSON.stringify
({
	type     :"Employee",
	email    :"a@a",
	password :"azerty",
	status   :"connected"
});


describe("Given I am connected as an employee", () => {
	describe("When I am on Bills Page", () => {


		beforeEach(() =>
		{
			Object.defineProperty(window, "localStorage", { value: localStorageMock });
			window.localStorage.setItem("user", USER);

			Object.defineProperty(window, "location", {
				value: {
					hash: ROUTES_PATH["Bills"]
				}
			});
		})

		test("Then bill icon in vertical layout should be highlighted", async () => {
			document.body.innerHTML = `<div id="root"></div>`;
			await Router();

			const icon = screen.getByTestId('icon-window');
			expect(icon.classList.contains("active-icon")).toBeTruthy();
		});



		test("Then bills should be ordered from earliest to latest", () => {
			const html = BillsUI({ data: bills });
			document.body.innerHTML = html;
			const dates = screen
				.getAllByText(
					/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/i
				)
				.map(a => a.innerHTML);
			const chrono = (a, b) => (a < b ? -1 : 1);
			const datesSorted = [...dates].sort(chrono);
			expect(dates).toEqual(datesSorted);
		});



		test("Then clicking the eye icon should open the modal.", async () => {
			document.body.innerHTML = `<div id="root"></div>`;
			await Router();
			$( ".icon-eye" ).first().click();

			// wait for the modal to open.
			//
			await new Promise( resolve =>
			{
				$('#modaleFile').on('shown.bs.modal', resolve );
			})

			const modal = screen.getByTestId('modaleFile');
			expect(modal.classList.contains("show")).toBeTruthy();
		});
	});



	test("fetches bills from an API and fails with 404 message error", async () => {
		const getSpy = jest.spyOn(firebase, "get");

		firebase.get.mockImplementationOnce(() =>
			Promise.reject(new Error("Erreur 404"))
		);
		const html = BillsUI({ error: "Erreur 404" });
		document.body.innerHTML = html;
		const message = await screen.getByText(/Erreur 404/);
		expect(message).toBeTruthy();
	});

	test("fetches messages from an API and fails with 500 message error", async () => {
		const getSpy = jest.spyOn(firebase, "get");

		firebase.get.mockImplementationOnce(() =>
			Promise.reject(new Error("Erreur 500"))
		);
		const html = BillsUI({ error: "Erreur 500" });
		document.body.innerHTML = html;
		const message = await screen.getByText(/Erreur 500/);
		expect(message).toBeTruthy();
	});
});
