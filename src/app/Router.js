import firestore from "./Firestore.js";
import Login, { PREVIOUS_LOCATION } from "../containers/Login.js";
import Bills from "../containers/Bills.js";
import NewBill from "../containers/NewBill.js";
import Dashboard from "../containers/Dashboard.js";

import BillsUI from "../views/BillsUI.js";
import DashboardUI from "../views/DashboardUI.js";
import LoadingPage from "../views/LoadingPage.js";

import { ROUTES, ROUTES_PATH } from "../constants/routes.js";


// TODO: what is localStorage? window.localStorage?

export default async () =>
{
	window.onpopstate = onPopstate;

	await onNavigate(window.location.hash);
};



async function onNavigate(hash, push = true)
{
	if( push )

		window.history.pushState( { hash }, hash, window.location.origin + hash )
	;

	const rootDiv = document.getElementById("root");

	switch( hash )
	{
		case ROUTES_PATH["Bills"]    : await createBills    ( rootDiv, hash ); break;
		case ROUTES_PATH["Dashboard"]: await createDashboard( rootDiv, hash ); break;
		case ROUTES_PATH["NewBill"]  :       createNewBill  ( rootDiv       ); break;

		default:	createLogin(rootDiv); break;
	}
};


async function createLogin(rootDiv)
{
	rootDiv.innerHTML = ROUTES({ pathname: ROUTES_PATH["Login"] });

	new Login({ document, localStorage, onNavigate, PREVIOUS_LOCATION, firestore });

	// TODO: move this to CSS.
	//
	document.body.style.backgroundColor = "#0E5AE5";
}


function createNewBill( rootDiv )
{
	rootDiv.innerHTML = ROUTES({ pathname: ROUTES_PATH["NewBill"] });

	new NewBill({ document, onNavigate, firestore, localStorage });

	document.getElementById("layout-icon1").classList.remove("active-icon");
	document.getElementById("layout-icon2").classList.add   ("active-icon");

	document.body.style.backgroundColor = "#fff";
}



async function createBills( rootDiv )
{
	document.body.style.backgroundColor = "#fff";
	rootDiv.innerHTML = LoadingPage();

	const bills = new Bills({ document, onNavigate, firestore, localStorage });

	let data;

	try
	{
		data = await bills.getBills();
	}

	catch( error )
	{
		console.error(error);
		rootDiv.innerHTML = ROUTES({ pathname: ROUTES_PATH["Bills"], error });
	}


	rootDiv.innerHTML = BillsUI({ data });

	document.getElementById("layout-icon1").classList.add   ( "active-icon" );
	document.getElementById("layout-icon2").classList.remove( "active-icon" );


	bills.setupPage();
}



async function createDashboard( rootDiv, pathname )
{
	document.body.style.backgroundColor = "#fff";
	rootDiv.innerHTML = LoadingPage();

	const dashboard = new Dashboard({ document, onNavigate, firestore, localStorage });

	let bills;
	try{ bills = await dashboard.getBillsAllUsers(); }

	catch( error )
	{
		console.error(error);
		rootDiv.innerHTML = ROUTES({ pathname, error });
	}

	rootDiv.innerHTML = DashboardUI({ data: { bills } });

	dashboard.setupPage();
}



function onPopstate( e )
{
	const user = JSON.parse( localStorage.getItem("user") );


	if( user )
	{
		onNavigate( e.state.hash, false /* dont push state */ );
	}

	else
	{
		rootDiv.innerHTML = ROUTES({ pathname: ROUTES_PATH["Login"] });
	}
};
