
import { bills as data } from "../../fixtures/bills.js";


class Document
{
	#data;

	constructor(data)
	{
		this.#data = data;
	}

	data()
	{
		return this.#data;
	}
}



class Firestore
{
	docs = [];
	uploaded;
	path;
	bill;

	storage = {
		ref: path =>
		{
			this.path = path;

			return { put: async file =>
			{
				this.uploaded = file;

				return {
					ref: {
						getDownloadURL: () => this.path
					}
				}
			}}
		}
	}

	constructor()
	{
		for( const d of data )
		{
			this.docs.push( new Document(d) );
		}
	}

	bills() { return this; }

	async get()
	{
		return this;
	}


	docs() { return this.docs; }


	async add(bill)
	{
		this.bill = bill;
	}
}

export default new Firestore;
