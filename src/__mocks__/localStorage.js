export const localStorageMock = (function () {
	let store = {};
	return {
		getItem: function (key) {
			return JSON.parse(store[key]);
		},
		setItem: function (key, value) {
			store[key] = JSON.stringify(value);;
		},
		clear: function () {
			store = {};
		},
		removeItem: function (key) {
			delete store[key];
		},
	};
})();
